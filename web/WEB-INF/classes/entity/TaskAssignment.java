/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author sasan
 */
public class TaskAssignment {
    private int workerID;
    private int tAID;   
    private String typeTask;
    private String codeTask;
    private String descriptionTask;
    private String dueDate;
    
    private int ownerID;
    private String ownerNAme;
    private String emailOwner;
    private String telOwner;
    
    private String comment;
    private int state;
    private String price;
    private String location;
    private String customerType;
    private String range;

    public TaskAssignment() {
    }

    public TaskAssignment(int workerID, int tAID, String typeTask, String codeTask, String descriptionTask, String dueDate, int ownerID, String ownerNAme, String emailOwner, String telOwner, String comment, int state, String price, String location, String customerType, String range) {
        this.workerID = workerID;
        this.tAID = tAID;
        this.typeTask = typeTask;
        this.codeTask = codeTask;
        this.descriptionTask = descriptionTask;
        this.dueDate = dueDate;
        this.ownerID = ownerID;
        this.ownerNAme = ownerNAme;
        this.emailOwner = emailOwner;
        this.telOwner = telOwner;
        this.comment = comment;
        this.state = state;
        this.price = price;
        this.location = location;
        this.customerType = customerType;
        this.range = range;
    }

    
    
    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }
    
    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }  

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    
    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    
    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }



    public int getWorkerID() {
        return workerID;
    }

    public void setWorkerID(int workerID) {
        this.workerID = workerID;
    }

    public int gettAID() {
        return tAID;
    }

    public void settAID(int tAID) {
        this.tAID = tAID;
    }

    public String getTypeTask() {
        return typeTask;
    }

    public void setTypeTask(String typeTask) {
        this.typeTask = typeTask;
    }

    public String getCodeTask() {
        return codeTask;
    }

    public void setCodeTask(String codeTask) {
        this.codeTask = codeTask;
    }

    public String getDescriptionTask() {
        return descriptionTask;
    }

    public void setDescriptionTask(String descriptionTask) {
        this.descriptionTask = descriptionTask;
    }

    public int getOwnerID() {
        return ownerID;
    }

    public void setOwnerID(int ownerID) {
        this.ownerID = ownerID;
    }

    public String getOwnerNAme() {
        return ownerNAme;
    }

    public void setOwnerNAme(String ownerNAme) {
        this.ownerNAme = ownerNAme;
    }

    public String getEmailOwner() {
        return emailOwner;
    }

    public void setEmailOwner(String emailOwner) {
        this.emailOwner = emailOwner;
    }

    public String getTelOwner() {
        return telOwner;
    }

    public void setTelOwner(String telOwner) {
        this.telOwner = telOwner;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
    
    
    
}
