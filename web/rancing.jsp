<%-- 
    Document   : availableJob
    Created on : Nov 10, 2016, 1:04:26 PM
    Author     : sasan
--%>

<%@page import="java.text.DecimalFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList" %>
<%@page import="entity.*" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

        <h1>Please rank to the tasks that the people have done for you</h1>

        <form action="RecordeRanking" method="POST">

            <table width="600" border="1">


                <tr>
                    <%-- <td witdth="250" valign="top">  Task ID</td> --%>
                     <td witdth="350" valign="top">The person who worked for </td>
                    <td witdth="250" valign="top">Type</td>  
                    <%--  <td witdth="250" valign="top">Code</td> --%>
                    <td witdth="250" valign="top">Description</td>  
                    <%--  <td witdth="400" valign="top">Owner ID</td> --%>          
                    <%--  <td witdth="500" valign="top"> Name of Task's Owner</td>    
                      <td witdth="500" valign="top"> Email of Task's Owner</td> 
                      <td witdth="500" valign="top"> Tel of Task's Owner</td> --%>

                    <td witdth="250" valign="top"> Due Date</td> 
                    <td witdth="250" valign="top"> Price</td> 
                    <td witdth="250" valign="top"> Location</td> 
                    <%--   <td witdth="250" valign="top"> Owner's Rate</td>  --%> 
                    <td witdth="250" valign="top">Average Rank</td>
                    <td witdth="300" valign="top">Rating(between 1 and 10)</td>  


                </tr>
                <%
                    ArrayList<Task> tasksRanking = new ArrayList<Task>();
                    tasksRanking = (ArrayList<Task>) session.getAttribute("tasksRAncing");
                    int i = -1;

                    for (Task t : tasksRanking) {


                %>
                <tr>

                     <td>
                        <% out.println(t.getComment());   %> 
                    </td>

                    
                    <td>
                        <% out.println(t.getType());   %> 
                    </td>

                    <td>
                        <%
                            out.println(t.getDescription());
                        %>
                    </td>
                    <%--   <td>
                           <% out.println(t.getOwnerID());   %>
                       </td> --%>
                    <td>
                        <% out.println(t.getDueDate());   %>
                    </td>
                    <td>
                        <% out.println(t.getPrice());   %>
                    </td>
                    <td>
                        <% out.println(t.getLocation());   %>
                    </td>

                    <td>
                        <% float r = (float) (Math.random() * 10);
                            float rr = Float.parseFloat(new DecimalFormat("##.##").format(r));
                            out.println(rr);
                            //  out.println(t.getRange());  
                        %>
                    </td>

                    <td>

                        <input type="TEXT" value="<%= ++i%>" name="rating">

                    </td>

                </tr>
                <%}%>
            </table> 

            <input type="SUBMIT" name="sendRanc" value="Send your Rank"></input>

        </form>

    </body>
</html>
