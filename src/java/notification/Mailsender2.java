/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notification;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;

public class Mailsender2
{

   
    public static void sendmail(String[] recipients, String sender, String subject, String contents)
	{
		final String username = "csci8700.acdc@gmail.com";
		final String password = "CSCI8700SoftwareRequirements#9";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		  });
		
		for(int i = 0; i < recipients.length; i++)
		{
			try
			{
				// Create a default MimeMessage object.
				MimeMessage message = new MimeMessage(session);

				// Set From: header field of the header.
				message.setFrom(new InternetAddress(sender));

				// Set To: header field of the header.
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipients[i]));

				// Set Subject: header field
				message.setSubject(subject);

				// Now set the actual message
				message.setText(contents);

				// Send message
				Transport.send(message);
				System.out.println("Sent message successfully....");
			}
			catch (MessagingException mex)
			{
				mex.printStackTrace();
			}
		}
	}
    
}
