/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import database.InsertUserInformation;
import database.Search;
import entity.Task;
import entity.TaskAssignment;
import entity.USerInformation;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author sasan
 */
public class LookingForJobControler extends HttpServlet {

    HttpSession session = null;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LookingForJobControler</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LookingForJobControler at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    public void service(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        session = request.getSession(true);
        ArrayList<TaskAssignment> tasks = new ArrayList<TaskAssignment>();
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        String type = request.getParameter("typeSearch"); 
        String code = request.getParameter("codeSearch");
        int state=Integer.parseInt(request.getParameter("typeState"));
        
        session.setAttribute("state", state);
        

        tasks = Search.lookingForJob(type, code,state, session);
        for (TaskAssignment t : tasks) {
            out.println("<html><B> Name:</B> </html>");
             out.print(t.getWorkerID());
            out.print(t.gettAID());
            out.print(t.getTypeTask());
            out.print(t.getCodeTask());
            out.print(t.getDescriptionTask());
            out.print(t.getOwnerID());
            out.print(t.getOwnerNAme());
            out.print(t.getEmailOwner());
            out.print(t.getTelOwner());
            out.print(t.getComment());

            out.println("<html><br><br/> </html>");
        }
        

        
        if(!tasks.isEmpty()){
            out.println("<html><h1><B> The Task with following information are availeble </B></h1> </html>");
        out.println("<h1><html><B> Select evey one that you can do it. </B> </h1></html>");
            String view = "/availableJob.jsp";               
                getServletContext().getRequestDispatcher(view).forward(request,response);
        } else
             out.println("<html><h1><B> The Task with following information are NOt availeble </B></h1> </html>");

        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
