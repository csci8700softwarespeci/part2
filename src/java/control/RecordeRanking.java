/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import database.Search;
import database.Update;
import entity.Task;
import entity.TaskAssignment;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author sasan
 */
public class RecordeRanking extends HttpServlet {

    HttpSession session = null;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet RecordeRanking</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet RecordeRanking at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    public void service(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //Double.parseDouble(new DecimalFormat("##.##").format(this));
        session = request.getSession(true);
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String[] selects = request.getParameterValues("rating");
        ArrayList<Task> tasksRanking = new ArrayList<Task>();
        tasksRanking = (ArrayList<Task>) session.getAttribute("tasksRAncing");

        out.println("<html><B><h1> Thank you! for your fedback</h1></B> </html>");
        out.println("<html><B><h1> Your fedback help other to find the correct person.</h1></B> </html>");
        // System.out.print(userName + "   " + password);
        int i = 0;
        for (String s : selects) {

            tasksRanking.get(i).setRange(s);
            Update.updateRanking(s, tasksRanking.get(i).getId());
            ++i;
        }

        out.println("<html>");
        out.println("<body bgcolor=\"Blue\">");
        out.println("<head>");

        out.println("<title> </title>");
        out.println("</head>");
        out.println("<body>");
        out.print("<form action=\"/CSCI8700/welcom.jsp\"  \"method=POST>\"  ");

        out.println("<br>");
        out.println("<input type=submit value=\"OK\">");
        out.println("</form>");
        out.println("</body>");
        out.println("</html>");

        out.close();

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
