/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import database.Search;
import entity.Task;
import entity.TaskAssignment;
import entity.USerInformation;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author sasan
 */
public class RancingControlForOwner extends HttpServlet {

    HttpSession session = null;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet RancingControlForOwner</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet RancingControlForOwner at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

        public void service(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        session = request.getSession(true);      
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        
        
       
        
        

         USerInformation usINFO = ( USerInformation)session.getAttribute("UInformation"); 
       
       

         ArrayList<Task> tasksRAncing = Search.lookingForRanking(usINFO.getId(), 3, session);
        for (Task t : tasksRAncing) {
            out.println("<html><B> Name:</B> </html>");
             out.print(t.getCode());
            out.print(t.getDescription());
            out.print(t.getDueDate());
            out.print(t.getOwnerID());
            out.print(t.getRange());
            out.print(t.getOwnerID());
            out.print(t.getComment());

            out.println("<html><br><br/> </html>");
        }
        

        
        if(tasksRAncing.size()>0){
             out.println("<html><h1><B> The Task with following information are availeble </B></h1> </html>");
        out.println("<h1><html><B> Select evey one that you can do it. </B> </h1></html>");
            String view = "/rancing.jsp";               
                getServletContext().getRequestDispatcher(view).forward(request,response);
        } else
            out.println("<h1><html><B> You don't have any Compelte task yet! </B> </h1></html>");
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
