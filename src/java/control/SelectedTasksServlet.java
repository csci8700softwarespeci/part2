/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import database.Search;
import database.Update;
import entity.TaskAssignment;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author sasan
 */
public class SelectedTasksServlet extends HttpServlet {
    
    HttpSession session=null;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SelectedTasksServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SelectedTasksServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

     public void service(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

       
        session= request.getSession(true);
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String[] selects=request.getParameterValues("selected");
        ArrayList<TaskAssignment> taskAssignments=Search.taskAssignments;
        
        out.println("<html><B><h1> These Jobs have been selected for you!</h1></B> </html>");
        out.println("<html><B><h1> We are sure that you can do it :) !</h1></B> </html>");
       // System.out.print(userName + "   " + password);
        for( String s: selects){
            if(taskAssignments.get(Integer.parseInt(s)).getState()==1)
                Update.update(2,taskAssignments.get(Integer.parseInt(s)).gettAID());
            else
                 Update.update(3,taskAssignments.get(Integer.parseInt(s)).gettAID());
            System.out.println(taskAssignments.get(Integer.parseInt(s)).gettAID());
        }
        
        out.println("<html>");
            out.println("<body bgcolor=\"Blue\">");
            out.println("<head>");

            out.println("<title> </title>");
            out.println("</head>");
            out.println("<body>");
            out.print("<form action=\"/CSCI8700/welcom.jsp\"  \"method=POST>\"  ");

            out.println("<br>");
            out.println("<input type=submit value=\"OK\">");
            out.println("</form>");
            out.println("</body>");
            out.println("</html>");

        out.close();

    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
