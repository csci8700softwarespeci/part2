/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import database.InsertUserInformation;
import database.Search;
import entity.Task;
import entity.USerInformation;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import notification.Mailsender2;

/**
 *
 * @author sasan
 */
public class TaskControlerServlet extends HttpServlet {

    HttpSession session = null;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet TaskControlerServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet TaskControlerServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    public void service(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        session = request.getSession(true);
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        USerInformation user = (USerInformation) session.getAttribute("UInformation");
//            System.out.println(user.getId());
//            System.out.println(user.getUserName());
//            System.out.println(user.getfName());
//            System.out.println(user.getlName());

        System.out.println(user.getId());
        System.out.println(request.getParameter("type2"));
        System.out.println(request.getParameter("code"));
        System.out.println(request.getParameter("describe"));
        System.out.println(request.getParameter("state"));
        System.out.println(request.getParameter("duedate"));

        Task task = new Task(user.getId(),
                request.getParameter("type2"),
                request.getParameter("code"),
                request.getParameter("describe"),
                1,
                request.getParameter("duedate"),
                0,//state
                "no comment",
                request.getParameter("price"),
                request.getParameter("location"),
                "0" //ranke
        );
        if (InsertUserInformation.insertTask(task)) {

            out.println("<html><h1><B> This job has been Recorded succesfuly </B></h1> </html>");
            
            out.println("<html><B>Naem of the owner of task: </B> </html>");
            out.println(user.getlName());
            out.println("<html><br><br/> </html>");
            out.println("<html><B>Type of task: </B> </html>");
            out.println(task.getType());
            out.println("<html><br><br/> </html>");
            out.println("<html><B>Code of task: </B> </html>");
            out.println(task.getCode());
            out.println("<html><br><br/> </html>");
            out.println("<html><B>Description about task: </B> </html>");
            out.println(task.getDescription());
            out.println("<html><br><br/> </html>");
            out.println("<html><B>State of task: </B> </html>");
            out.println(task.getState());
            out.println("<html><br><br/> </html>");
            out.println("<html><B>Due Date: </B> </html>");
            out.println(task.getDueDate());
            out.println("<html><br><br/> </html>");
            out.println("<html><B>Price: </B> </html>");
            out.println(task.getPrice());
            out.println("<html><br><br/> </html>");
            out.println("<html><B>Location: </B> </html>");
            out.println(task.getLocation());
            out.println("<html><br><br/> </html>");

            out.println("<html>");
            out.println("<body bgcolor=\"Blue\">");
            out.println("<head>");

            out.println("<title> </title>");
            out.println("</head>");
            out.println("<body>");
            out.print("<form action=\"/CSCI8700/welcom.jsp\"  \"method=POST>\"  ");

            out.println("<br>");
            out.println("<input type=submit value=\"OK\">");
            out.println("</form>");
            out.println("</body>");
            out.println("</html>");

//            String[] m = new String[2];
//            m[0] = "sazizian@unomaha.edu";
//            m[1] = "sasan.azizian@gmail.com";
//            Mailsender2.sendmail(m, "csci8700.acdc@gmail.com", "new Task", task.getType());
            

        } else {
            out.println("<html><B>Please try again you taske does not generate </B> </html>");
        }

        out.close();

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
