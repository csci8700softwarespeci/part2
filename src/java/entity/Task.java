/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author sasan
 */
public class Task {
    private int Id;
    private int ownerID;
    private String type;
    private String code;
    private String description;
    private int state; // 0: new task, 1= in progress, 2=finished, 3=cancle 
    private String dueDate;
    private int workerID;
    private String comment;
    private String price;
    private String location;
    private String range;

    public Task() {
    }

    public Task(int Id, int ownerID, String type, String code, String description, int state, String dueDate, int workerID, String comment, String price, String location, String range) {
        this.Id = Id;
        this.ownerID = ownerID;
        this.type = type;
        this.code = code;
        this.description = description;
        this.state = state;
        this.dueDate = dueDate;
        this.workerID = workerID;
        this.comment = comment;
        this.price = price;
        this.location = location;
        this.range = range;
    }

    public Task(int ownerID, String type, String code, String description, int state, String dueDate, int workerID, String comment, String price, String location, String range) {
        this.ownerID = ownerID;
        this.type = type;
        this.code = code;
        this.description = description;
        this.state = state;
        this.dueDate = dueDate;
        this.workerID = workerID;
        this.comment = comment;
        this.price = price;
        this.location = location;
        this.range = range;
    }



    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
    

    public int getWorkerID() {
        return workerID;
    }

    public void setWorkerID(int workerID) {
        this.workerID = workerID;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

   

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }
    

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public int getOwnerID() {
        return ownerID;
    }

    public void setOwnerID(int ownerID) {
        this.ownerID = ownerID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    
}
