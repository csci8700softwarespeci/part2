/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author sasan
 */
public class USerInformation {
    private int Id;
    private String fName;
    private String lName;
    private String  address;
    private String zipcode;
    private String tel;
    private String state;
    private String email;
    private String userName;
    private String password;

    public USerInformation() {
    }

    public USerInformation(int Id, String fName, String lName, String address, String zipcode, String tel, String state, String email, String userName, String password) {
        this.Id = Id;
        this.fName = fName;
        this.lName = lName;
        this.address = address;
        this.zipcode = zipcode;
        this.tel = tel;
        this.state = state;
        this.email = email;
        this.userName = userName;
        this.password = password;
    }
    

    public USerInformation(String fName, String lName, String address, String zipcode, String tel, String state, String email, String userName, String password) {
        this.fName = fName;
        this.lName = lName;
        this.address = address;
        this.zipcode = zipcode;
        this.tel = tel;
        this.state = state;
        this.email = email;
        this.userName = userName;
        this.password = password;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    
    
    
    
    
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
    
    
    
}
