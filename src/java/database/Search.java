package database;


import entity.Task;
import entity.TaskAssignment;
import entity.USerInformation;
import java.sql.*;
import java.util.ArrayList;
import javax.servlet.http.HttpSession;


public class Search {

   
    static boolean f;
    public static ArrayList<TaskAssignment> taskAssignments=null;
    public static int IDuser;

    public static boolean checkPassword( String userName, String password,HttpSession session){

        Connection con= DBConnection.getConnection();
        Statement stmt=null;
        ResultSet rs=null;
        f=false;
        USerInformation uInfo=null;
       
        
   
        String sql="SELECT * FROM user_information";

        
        try{
            stmt=con.createStatement();
            rs=stmt.executeQuery(sql);
            while(rs.next()){
                //System.println()
                     if(rs.getString(9).trim().equals(userName) && rs.getString(10).trim().equals(password)){
                //     System.out.println("UserName="+ rs.getString(9) + "   Password= "+rs.getString(10)+"   "+rs.getInt(1));
                     
                     uInfo=new USerInformation(Integer.parseInt(rs.getString(1)),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),
                                                rs.getString(6),rs.getString(7),rs.getString(8),rs.getString(9),rs.getString(10));
                     session.setAttribute("UInformation", uInfo);
                     session.setAttribute("ID",rs.getInt(1));
                      session.setAttribute("NAME",rs.getString(2));
                      IDuser=Integer.parseInt(rs.getString(1));
                      
                     
                     f=true;
                 }
            }

        } catch(Exception e){
            e.printStackTrace();
        }


        finally {

              if (rs != null) try { rs.close(); } catch(Exception e) { System.out.println(e); }
              if (stmt != null) try { stmt.close(); } catch(Exception e) { System.out.println(e); }
              if (con != null) try { con.close(); } catch(Exception e) { System.out.println(e); }

          }
        
        return f;
    }
    
    public static   ArrayList<TaskAssignment> lookingForJob(String type,String code,int state,HttpSession session){
        
        Connection con= DBConnection.getConnection();
        Statement stmt=null;
        ResultSet rs=null;
        f=false;
        ArrayList<Task> tasks=new ArrayList<Task>();
        taskAssignments=new ArrayList<TaskAssignment>();
         USerInformation uInfo= new USerInformation();
         int ID=(Integer)session.getAttribute("ID");
         System.out.println("ID"+ID);
         USerInformation ownerInformation=null;
        
   
        String sql="SELECT * FROM task";

        
        try{
            stmt=con.createStatement();
            rs=stmt.executeQuery(sql);
            while(rs.next()){
                //System.println()
                     if(rs.getString(3).trim().equals(type) && rs.getString(4).trim().equals(code) && rs.getInt(6)==state ){
                //     System.out.println("UserName="+ rs.getString(9) + "   Password= "+rs.getString(10)+"   "+rs.getInt(1));
                     tasks.add(new Task(
                                        rs.getInt(1),
                                        rs.getInt(2),
                                        rs.getString(3),
                                        rs.getString(4),
                                        rs.getString(5),
                                        rs.getInt(6),
                                        rs.getString(7),
                                        rs.getInt(8),                                        
                                        rs.getString(9),
                                        rs.getString(10),
                                        rs.getString(11),
                                        rs.getString(12)
                                          
                     
                     ));
                     System.out.println("NAme===="+findOwner(rs.getInt(8)).getUserName());
                     ownerInformation=findOwner(1);
                   taskAssignments.add(new TaskAssignment(uInfo.getId(),
                           rs.getInt(1),  rs.getString(3),  rs.getString(4),  rs.getString(5), rs.getString(7),
                           rs.getInt(2), ownerInformation.getfName(), ownerInformation.getEmail(), ownerInformation.getTel(),
                           rs.getString(9),rs.getInt(6),rs.getString(10),rs.getString(11),ownerInformation.getState(),rs.getString(12)));
                   
                     session.setAttribute("tasks", taskAssignments);
                     session.setAttribute("IDTask",rs.getInt(1));
                      session.setAttribute("OwnerOFTask",rs.getString(2));
                      
                     
                     f=true;
                 }
            }

        } catch(Exception e){
            e.printStackTrace();
        }


        finally {

              if (rs != null) try { rs.close(); } catch(Exception e) { System.out.println(e); }
              if (stmt != null) try { stmt.close(); } catch(Exception e) { System.out.println(e); }
              if (con != null) try { con.close(); } catch(Exception e) { System.out.println(e); }

          }
        
        return taskAssignments;
    }
        
    public static USerInformation findOwner(int ownerID){
      Connection con= DBConnection.getConnection();
        Statement stmt=null;
        ResultSet rs=null;
       
        USerInformation uInfo=null;
        
   
        String sql="SELECT * FROM user_information";

        
        try{
            stmt=con.createStatement();
            rs=stmt.executeQuery(sql);
            while(rs.next()){
                //System.println()
                     if(rs.getInt(1)== ownerID ){
                //     System.out.println("UserName="+ rs.getString(9) + "   Password= "+rs.getString(10)+"   "+rs.getInt(1));
                     
                     uInfo=new USerInformation(Integer.parseInt(rs.getString(1)),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),
                                                rs.getString(6),rs.getString(7),rs.getString(8),rs.getString(9),rs.getString(10));

                 }
            }

        } catch(Exception e){
            e.printStackTrace();
        }


        finally {

              if (rs != null) try { rs.close(); } catch(Exception e) { System.out.println(e); }
              if (stmt != null) try { stmt.close(); } catch(Exception e) { System.out.println(e); }
              if (con != null) try { con.close(); } catch(Exception e) { System.out.println(e); }

          }
        
        return uInfo;
    }
    
        public static   ArrayList<Task> lookingForRanking(int ownerID,int state,HttpSession session){
        
        Connection con= DBConnection.getConnection();
        Statement stmt=null;
        ResultSet rs=null;
        f=false;
        ArrayList<Task> tasksRAncing=new ArrayList<Task>();       
            
    
     
        
   
        String sql="SELECT * FROM task";

        
        try{
            stmt=con.createStatement();
            rs=stmt.executeQuery(sql);
           
            while(rs.next()){
                //System.println()
                     if(rs.getInt(2)==ownerID && rs.getInt(6)==state ){
                //     System.out.println("UserName="+ rs.getString(9) + "   Password= "+rs.getString(10)+"   "+rs.getInt(1));
                     tasksRAncing.add(new Task(
                                        rs.getInt(1),
                                        rs.getInt(2),
                                        rs.getString(3),
                                        rs.getString(4),
                                        rs.getString(5),
                                        rs.getInt(6),
                                        rs.getString(7),
                                        rs.getInt(8),
                                        findOwner(rs.getInt(8)).getUserName(),
                                       // rs.getString(9),
                                        rs.getString(10),
                                        rs.getString(11),
                                        rs.getString(12)
                                          
                     
                     ));
                    
                    session.setAttribute("tasksRAncing", tasksRAncing);
                     
                     f=true;
                 }
            }

        } catch(Exception e){
            e.printStackTrace();
        }


        finally {

              if (rs != null) try { rs.close(); } catch(Exception e) { System.out.println(e); }
              if (stmt != null) try { stmt.close(); } catch(Exception e) { System.out.println(e); }
              if (con != null) try { con.close(); } catch(Exception e) { System.out.println(e); }

          }
        
        return tasksRAncing;
    }
    
}
