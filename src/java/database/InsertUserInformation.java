/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import database.*;
import entity.Task;
import entity.USerInformation;
import java.sql.Connection;
import java.sql.*;
import javax.jms.Session;
import javax.servlet.http.HttpSession;

/**
 *
 * @author sasan
 */
public class InsertUserInformation {

    public static boolean insert(USerInformation user) {
        Connection con = DBConnection.getConnection();
        boolean state=false;
        if (con != null) {
            System.out.println("You are connected to DB successfully!");
        } else {
            System.out.println("Sorry connection is null!");
        }

        PreparedStatement pstmt = null;

        try {

            String sql = "INSERT INTO user_information (fname, lname, address, zipcode, tel, state,email, userName, password ) VALUES (?,?,?,?,?,?,?,?,?)";
            pstmt = con.prepareStatement(sql);
            pstmt.setString(1, user.getfName());
            pstmt.setString(2, user.getlName());
            pstmt.setString(3, user.getAddress());
            pstmt.setString(4, user.getZipcode());
            pstmt.setString(5, user.getTel());
            pstmt.setString(6, user.getState());
            pstmt.setString(7, user.getEmail());
            pstmt.setString(8, user.getUserName());
            pstmt.setString(9, user.getPassword());
            pstmt.executeUpdate();
            state=true;
        } catch (SQLException e) {

            e.printStackTrace();

        } finally {           
            

            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                    System.out.println(e);
                }
            }

        }
        return state;
    }
    
    public static boolean insertTask(Task task){
        Connection con = DBConnection.getConnection();
        boolean state=false;
        if (con != null) {
            System.out.println("You are connected to DB successfully!");
        } else {
            System.out.println("Sorry connection is null!");
        }

        PreparedStatement pstmt = null;

        try {

            String sql = "INSERT INTO task (ownerID, type, code, description, state, duedate,workerID,comment,price,location,ranke ) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
            pstmt = con.prepareStatement(sql);
          //  pstmt.setString(1, user.getfName());
            pstmt.setInt(1, task.getOwnerID());
            pstmt.setString(2, task.getType());
            pstmt.setString(3, task.getCode());
            pstmt.setString(4, task.getDescription());
            pstmt.setInt(5,task.getState());
            pstmt.setString(6, task.getDueDate());
             pstmt.setInt(7,0);
             pstmt.setString(8, "no comment");
             pstmt.setString(9, task.getPrice());
              pstmt.setString(10, task.getLocation());
              pstmt.setString(11, task.getRange());

            pstmt.executeUpdate();
            state=true;
        } catch (SQLException e) {

            e.printStackTrace();

        } finally {           
            

            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                    System.out.println(e);
                }
            }

        }
        return state;
        
    }

}
