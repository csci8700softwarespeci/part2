/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import static database.Search.f;
import entity.Task;
import entity.TaskAssignment;
import entity.USerInformation;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.servlet.http.HttpSession;

/**
 *
 * @author sasan
 */
public class Update {

    public static boolean update(int state, int taskID) {

        Connection con = DBConnection.getConnection();
        Statement stmt = null;
        ResultSet rs = null;
        f = false;
        USerInformation uInfo = null;

        String sql = "UPDATE task SET state=?, workerID=? where taskID=?";

        try {
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setInt(1, state);
            pst.setInt(2,Search.IDuser);
            pst.setInt(3, taskID);
            pst.execute();
            f=true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                    System.out.println(e);
                }
            }

        }

        return f;
    }
    
    public static boolean updateRanking(String ranke, int taskID) {

        Connection con = DBConnection.getConnection();
        Statement stmt = null;
        ResultSet rs = null;
        f = false;       

        String sql = "UPDATE task SET ranke=? where taskID=?";

        try {
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setString(1, ranke); 
            pst.setInt(2, taskID);
            pst.execute();
            f=true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                    System.out.println(e);
                }
            }

        }

        return f;
    }

  
}
